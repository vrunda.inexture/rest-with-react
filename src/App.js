import React, { useEffect, useState } from 'react';
import Card from './components/Card';
import axios from 'axios';

const App = () => {
  const [newsArticles, setNewsArticles] = useState([]);

  // fetch the news from the api
  useEffect(() => {
    fetch(
      'https://newsapi.org/v2/everything?q=apple&from=2022-02-15&to=2022-02-15&sortBy=popularity&apiKey=9f831e840ff74928b528b551218fa6e6'
    )
      .then((res) => res.json())
      .then((newsArticles) => {
        console.log(newsArticles);
        setNewsArticles(newsArticles.articles);
      });
  }, []);

  useEffect(() => {
    axios.get('https://reqres.in/api/users?page=2').then((res) => {
      console.log(res.data);
      console.log('User Fetched!');
    });
  }, []);

  useEffect(() => {
    axios
      .post('https://reqres.in/api/users', {
        name: 'vrunda',
        job: 'Web Developer',
      })
      .then((res) => {
        console.log(res.data);
        console.log('User Created!');
      });

    axios
      .put(`https://reqres.in/api/users/2`, {
        name: 'vrunda',
        job: 'Web Developer',
      })
      .then((res) => {
        console.log(res.data);
        console.log('User Updated!');
      });

    axios.delete('https://reqres.in/api/users/2').then((res) => {
      console.log(res.data);
      console.log('User Deleted!');
    });
  }, []);

  return (
    <div className='App'>
      <div className='container'>
        <div className='row'>
          <h1 className='text-center'>Latest News</h1>
          <hr />
          {/* loop through all the articles */}
          {newsArticles.map((newsArticle, index) => {
            return <Card key={index} newsArticle={newsArticle} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default App;
