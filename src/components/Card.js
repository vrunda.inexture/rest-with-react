import React from 'react';

const Card = ({ newsArticle }) => {
  const { publishedAt, title, source, content, urlToImage, url } = newsArticle;

  // converting the string to date
  const publishedDate = new Date(publishedAt).toString().slice(0, 16);

  return (
    <div className='col-md-4 my-3'>
      <div className='card h-100'>
        <img
          src={urlToImage}
          className='card-img-top'
          alt='news'
          style={{ height: '200px' }}
        />
        <div className='card-header'>{source.name}</div>
        <div className='card-body'>
          <h5 className='card-title'>{title}</h5>
          <p className='card-text'>{content}</p>
          <a href={url} className='btn btn-primary'>
            Read More{' '}
          </a>
        </div>
        <div className='card-footer text-muted'>{publishedDate}</div>
      </div>
    </div>
  );
};

export default Card;
